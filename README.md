# Listings in the book "Homogenization methods" by Rainer Glüge

## RVEs

All RVE-scripts are contained in [RVE-scripts](RVE-scripts)

## Characterization of microstructure

[Generating the two-point correlations on a binary microstructure.](scripts/Listing-01_en.nb)

[Numerical experiment to approach the probability of the needle not intersecting the grid lines.](scripts/Listing-02_en.nb)

## Homogenization with the aid of structural mechanics

[Homogenization of the honeycomb structure using beam theory.](scripts/Listing-03_en.nb)

[Nonlinear homogenization of the spring model.](scripts/Listing-04_en.nb)

[Linearizing the nonlinear effective stress-strain relation of the spring structure.](scripts/Listing-05_en.nb)

## Estimates in terms of volume fractions

[Simple averages of dimensionless moduli 1 and 20.](scripts/Listing-06_en.nb)

## Basic solutions

[Explicit calculation of the laminate stiffness from the symbolic expression.](scripts/Listing-07_en.nb)

[Calculation of the laminate stiffness from the jump balances, material laws and mixture rules.](scripts/Listing-08_en.nb)

## Improved estimates based on the Eshelby solution

[Generating the dilute distribution estimate and the Mori-Tanaka-estimate.](scripts/Listing-09_en.nb)

[Differential scheme estimate.](scripts/Listing-11_en.nb)

[Self-consistent estimate.](scripts/Listing-12_en.nb)

## Hashin-Shtrikman variational method

[Hashin-Shtrikman bounds.](scripts/Listing-13_en.nb)

[Hashin-Shtrikman bounds for vector field problems.](scripts/Listing-14_en.nb)

## Fourier- and Green methods

[Integrating k o k o k o k and k o I o k over the unit sphere.](scripts/Listing-15_en.nb)

[Laminate conductivity with Fourier methods.](scripts/Listing-17_en.nb)

[Fourier method 1: Estimate the effective elasticity by solving a humongous linear system.](scripts/Listing-19_en.nb)

[Fixed point iteration for a simple ordinary differential equation.](scripts/Listing-24_en.nb)

[Fixed point iteration for the 3D conductivity, implemented in Mathematica.](scripts/Listing-25_en.nb)

[Fixed point iteration for the 3D conductivity, implemented in Matlab / Octave.](scripts/Listing-26_en.m)

## Orientation averages

[Generating symmetry groups from generators and applying them to reduce the number of independent components of the elasticity tensor.](scripts/Listing-27_en.nb)

[Showing the rank reduction up to the n-fold symmetry for tensors of order n.](scripts/Listing-28_en.nb)

[Calculating the Jacobian determinant needed for integration over SO(3) when parameterized by Euler angles or Rodrigues parameters.](scripts/Listing-29_en.nb)

[Generating homogeneous discrete orientation distributions, the hard way.](scripts/Listing-31_en.nb)

[Generating homogeneous discrete orientation distributions, the easy way.](scripts/Listing-32_en.nb)

[Integrating over SO(3) to obtain the orientation average and the corresponding isotropic projector.](scripts/Listing-33_en.nb)

[Homogenizing the heat conductivity of crystalline black phosphor in case of an isotropic orientation distribution.](scripts/Listing-34_en.nb)

[Homogenizing the elastic properties of isotropically distributed iron single crystals.](scripts/Listing-35_en.nb)

[Hashin-Shtrikman-bounds for the isotropic orientation distribution of cubic single crystals, evaluated for iron.](scripts/Listing-37_en.nb)

