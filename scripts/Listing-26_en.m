# This is an implementation of a spectral solver. It solves the heat conduction DE 
#
#   q(x).V = 0        (I)   V = nabla
#
# with 
#
#   q(x)=L(x).g(x)    (II)
#
# with the average of g(x) prescribed.
# For each iteration q.V is calculated for (I) in Fourier space. 
# (II) is evaluated in real space. The update of g(x) is
#
#   Delta_g = (q_hat.k) / (k.L0.k) k
#
# with L0 the reference conductivity l0 * identity matrix. 
# L0 should not be too far from the volume average ||L(x)||. 
# k is the wave vector. 

clear;
clc;

page_output_immediately(1);

# size of the periodically repeated unit cell
size       = 20;

# initialize vector field
g          = zeros(3,size,size,size);
q          = zeros(3,size,size,size);
qdach      = zeros(3,size,size,size);
deltagdach = zeros(3,size,size,size);

# set the conductivities
l0   = 1.5;
L0   = eye(3,3).*l0;
L1   = 1;
L2   = 2;

# precision goal
tol  = 1e-8;

# The effective conductivities requires to prescribe the effective 
# temperature gradient along three orthogonal directions.
# The field g is initialized homogeneously
for ii = 1:3
  gmean       = zeros(3,1);
  gmean(ii,1) = 1;
  for jj = 1:size
    for kk = 1:size
      for ll = 1:size
          g(1:3,jj,kk,ll)=gmean;
      end
    end
  end

  
# Start of the iteration
  residuum = 1;
  while residuum > tol
# determine local heat flux
    for jj = 1:size
      for kk = 1:size
        for ll = 1:size
# The if clause encodes the microstructure, i.e. the value of L(x), 
# here as a laminate.          
          if jj <= size / 2
            q(1:3,jj,kk,ll) = L1*g(1:3,jj,kk,ll);
          elseif jj > size /2 
            q(1:3,jj,kk,ll) = L2*g(1:3,jj,kk,ll);
          end  
        end
      end
    end

# Fourier transform of q
    qdach(1,:,:,:) = fftn(q(1,:,:,:))./sqrt(size*size*size);
    qdach(2,:,:,:) = fftn(q(2,:,:,:))./sqrt(size*size*size);
    qdach(3,:,:,:) = fftn(q(3,:,:,:))./sqrt(size*size*size);
    
  
# Calculation of the divergence by multiplication with k except for k = (0 0 0)
# which corresponds to the prescribed mean value.
    for jj = 1:size
      for kk = 1:size
        for ll = 1:size
          if (jj ~= 1 ) || (kk ~= 1) || (ll ~= 1) 
            kkk = [ jj-1;
                    kk-1;
                    ll-1 ];  
            deltagdach(:,jj,kk,ll) = ( kkk'*qdach(:,jj,kk,ll) )/( kkk'*L0*kkk )*kkk; 
          else
            deltagdach(:,jj,kk,ll) = zeros(3,1);
          end  
        end
      end
    end
        
# Fourier Backtransform     
    deltag(1,:,:,:) = ifftn(deltagdach(1,:,:,:)).*sqrt(size*size*size);
    deltag(2,:,:,:) = ifftn(deltagdach(2,:,:,:)).*sqrt(size*size*size);
    deltag(3,:,:,:) = ifftn(deltagdach(3,:,:,:)).*sqrt(size*size*size);

# Update    
    g=g-deltag;
    
# Calculate the proportional update sd/sg, where sg is scalar(g) and sd is scalar(delta g)
    sg=0;
    sd=0;
    for jj = 1:size
      for kk = 1:size
        for ll = 1:size
          sg = sg + sqrt( g(1,jj,kk,ll)^2 + g(2,jj,kk,ll)^2 + g(3,jj,kk,ll)^2 );
          sd = sd + sqrt( deltag(1,jj,kk,ll)^2 + deltag(2,jj,kk,ll)^2 + deltag(3,jj,kk,ll)^2 );
        end
      end
    end
    residuum=sd/sg
  end
  # The effective heat flux, which is the coefficient to k=(0 0 0), is extracted.
  qdach(:,1,1,1)./sqrt(size^3)
end
# The result is the effective laminate conductivity
# 
#          [  1.333            ]
# L_eff  = [         1.5       ]
#          [              1.5  ]
#
# i.e. the arithmetic mean of (1,2) parallel to the laminate and the harmonic mean 
# perpendicular to the laminate. 
