(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      8708,        280]
NotebookOptionsPosition[      7822,        257]
NotebookOutlinePosition[      8214,        273]
CellTagsIndexPosition[      8171,        270]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{"Remove", "[", "\"\<Global`*\>\"", "]"}], "\n", 
 RowBox[{
  RowBox[{"w2", "=", 
   RowBox[{"3", " ", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"K0", "+", 
       RowBox[{"2", " ", "G0"}]}], ")"}], "/", 
     RowBox[{"(", 
      RowBox[{"5", " ", "G0", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"3", " ", "K0"}], "+", 
         RowBox[{"4", " ", "G0"}]}], ")"}]}], ")"}]}]}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"B", "=", 
   RowBox[{
    RowBox[{
     RowBox[{"3", "/", "5"}], "/", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"1", "/", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"2", " ", "GC2"}], "-", 
          RowBox[{"2", " ", "G0"}]}], ")"}]}], "+", "w2"}], ")"}]}], "+", 
    RowBox[{
     RowBox[{"2", "/", "5"}], "/", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"1", "/", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"2", " ", "GC3"}], "-", 
          RowBox[{"2", " ", "G0"}]}], ")"}]}], "+", "w2"}], ")"}]}]}]}], 
  ";"}], "\n", 
 RowBox[{
  RowBox[{"kappa", "=", 
   RowBox[{"B", "/", 
    RowBox[{"(", 
     RowBox[{"1", "-", 
      RowBox[{"B", " ", "w2"}]}], ")"}]}]}], ";"}], "\n", 
 RowBox[{"GHS", "=", 
  RowBox[{
   RowBox[{"G0", "+", 
    RowBox[{"kappa", "/", "2"}]}], "//", "FullSimplify"}]}], "\n", 
 RowBox[{
  RowBox[{"GHSminus", "=", 
   RowBox[{"FullSimplify", "[", 
    RowBox[{"GHS", "/.", " ", 
     RowBox[{"{", 
      RowBox[{"G0", "->", "GC3"}], "}"}]}], "]"}]}], " ", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
     RowBox[{"For", " ", "iron", " ", "GC3"}], "<", "GC2"}], ",", 
    RowBox[{"therefore", " ", "\"\<minus\>\""}]}], "*)"}]}], "\n", 
 RowBox[{
  RowBox[{"GHSplus", "=", 
   RowBox[{"FullSimplify", "[", 
    RowBox[{"GHS", "/.", " ", 
     RowBox[{"{", 
      RowBox[{"G0", "->", "GC2"}], "}"}]}], "]"}]}], "\n", 
  RowBox[{"(*", 
   RowBox[{
   "Single", " ", "crystal", " ", "constants", " ", "for", " ", "iron"}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"K0", "=", 
    RowBox[{"500.8", "/", "3"}]}], ";"}], 
  RowBox[{"(*", 
   RowBox[{"usually", " ", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"c11", "+", 
       RowBox[{"2", " ", "c12"}]}], ")"}], "/", "3"}]}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"GC2", "=", "116.4"}], ";"}], 
  RowBox[{"(*", 
   RowBox[{"usually", " ", "2", " ", 
    RowBox[{"c44", "/", "2"}], " ", "when", " ", "a", " ", "nonnormalized", 
    " ", "basis", " ", "is", " ", "used"}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"GC3", "=", "48.36"}], ";"}], 
  RowBox[{"(*", 
   RowBox[{"usually", " ", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"c11", "-", "c12"}], ")"}], "/", "2"}]}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{"Print", "[", 
  RowBox[{
  "\"\<Bounds for the shear modulus: \>\"", ",", "GHSminus", ",", 
   "\"\< < G < \>\"", ",", "GHSplus"}], "]"}], "\n", 
 RowBox[{
  RowBox[{"EHSminus", "=", 
   RowBox[{"9", " ", "K0", " ", 
    RowBox[{"GHSminus", "/", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"3", " ", "K0"}], "+", "GHSminus"}], ")"}]}]}]}], 
  ";"}], "\n", 
 RowBox[{
  RowBox[{"EHSplus", "=", 
   RowBox[{"9", " ", "K0", " ", 
    RowBox[{"GHSplus", "/", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"3", " ", "K0"}], "+", "GHSplus"}], ")"}]}]}]}], ";"}], "\n", 
 RowBox[{"Print", "[", 
  RowBox[{
  "\"\<Bounds for Young's modulus: \>\"", ",", "EHSminus", ",", 
   "\"\< < E < \>\"", ",", "EHSplus"}], "]"}], "\n"}], "Input",
 CellChangeTimes->{{3.87473781846034*^9, 
  3.87473784804072*^9}},ExpressionUUID->"88ffafaa-3a80-4d6b-b5f7-\
5571aeb8fc6c"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   RowBox[{"4", " ", "G0", " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"6", " ", "G0", " ", "GC2"}], "+", 
      RowBox[{"4", " ", "G0", " ", "GC3"}], "+", 
      RowBox[{"15", " ", "GC2", " ", "GC3"}]}], ")"}]}], "+", 
   RowBox[{"3", " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"9", " ", "G0", " ", "GC2"}], "+", 
      RowBox[{"6", " ", "G0", " ", "GC3"}], "+", 
      RowBox[{"10", " ", "GC2", " ", "GC3"}]}], ")"}], " ", "K0"}]}], 
  RowBox[{
   RowBox[{"4", " ", "G0", " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"10", " ", "G0"}], "+", 
      RowBox[{"6", " ", "GC2"}], "+", 
      RowBox[{"9", " ", "GC3"}]}], ")"}]}], "+", 
   RowBox[{"3", " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"15", " ", "G0"}], "+", 
      RowBox[{"4", " ", "GC2"}], "+", 
      RowBox[{"6", " ", "GC3"}]}], ")"}], " ", "K0"}]}]]], "Output",
 CellChangeTimes->{3.8747378200607862`*^9},
 CellLabel->
  "Out[157]=",ExpressionUUID->"e6c86fb4-ffe9-45d7-bcae-dfbb6d5456bd"],

Cell[BoxData[
 FractionBox[
  RowBox[{"GC3", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"2", " ", "GC3", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"8", " ", "GC3"}], "+", 
        RowBox[{"9", " ", "K0"}]}], ")"}]}], "+", 
     RowBox[{"GC2", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"84", " ", "GC3"}], "+", 
        RowBox[{"57", " ", "K0"}]}], ")"}]}]}], ")"}]}], 
  RowBox[{
   RowBox[{"12", " ", "GC2", " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"2", " ", "GC3"}], "+", "K0"}], ")"}]}], "+", 
   RowBox[{"GC3", " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"76", " ", "GC3"}], "+", 
      RowBox[{"63", " ", "K0"}]}], ")"}]}]}]]], "Output",
 CellChangeTimes->{3.8747378201144753`*^9},
 CellLabel->
  "Out[158]=",ExpressionUUID->"32c1c35e-18d4-4712-ab26-1570918273a3"],

Cell[BoxData[
 FractionBox[
  RowBox[{"GC2", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"24", " ", 
      SuperscriptBox["GC2", "2"]}], "+", 
     RowBox[{"76", " ", "GC2", " ", "GC3"}], "+", 
     RowBox[{"27", " ", "GC2", " ", "K0"}], "+", 
     RowBox[{"48", " ", "GC3", " ", "K0"}]}], ")"}]}], 
  RowBox[{
   RowBox[{"64", " ", 
    SuperscriptBox["GC2", "2"]}], "+", 
   RowBox[{"36", " ", "GC2", " ", "GC3"}], "+", 
   RowBox[{"57", " ", "GC2", " ", "K0"}], "+", 
   RowBox[{"18", " ", "GC3", " ", "K0"}]}]]], "Output",
 CellChangeTimes->{3.8747378201513577`*^9},
 CellLabel->
  "Out[159]=",ExpressionUUID->"4f4c957f-32b9-4e5b-bcf8-70fbcfa30e56"],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Bounds for the shear modulus: \"\>", "\[InvisibleSpace]", 
   "80.85093005747252`", "\[InvisibleSpace]", "\<\" < G < \"\>", 
   "\[InvisibleSpace]", "83.44752181930349`"}],
  SequenceForm[
  "Bounds for the shear modulus: ", 80.85093005747252, " < G < ", 
   83.44752181930349],
  Editable->False]], "Print",
 CellChangeTimes->{3.8747378201543007`*^9},
 CellLabel->
  "During evaluation of \
In[153]:=",ExpressionUUID->"9fa8dbf7-34c1-447e-a978-6f2a72943bda"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Bounds for Young's modulus: \"\>", "\[InvisibleSpace]", 
   "208.83734735255098`", "\[InvisibleSpace]", "\<\" < E < \"\>", 
   "\[InvisibleSpace]", "214.58637323941713`"}],
  SequenceForm[
  "Bounds for Young's modulus: ", 208.83734735255098`, " < E < ", 
   214.58637323941713`],
  Editable->False]], "Print",
 CellChangeTimes->{3.874737820156014*^9},
 CellLabel->
  "During evaluation of \
In[153]:=",ExpressionUUID->"9a32af68-4aaf-4a63-b9c2-269feefdab7c"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1440., 746.25},
WindowMargins->{{0, Automatic}, {0, Automatic}},
FrontEndVersion->"12.3 for Linux x86 (64-bit) (May 11, 2021)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"40b07383-4eab-4055-b714-a3acc8aa5eb4"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 3656, 120, 341, "Input",ExpressionUUID->"88ffafaa-3a80-4d6b-b5f7-5571aeb8fc6c"],
Cell[4239, 144, 1023, 30, 50, "Output",ExpressionUUID->"e6c86fb4-ffe9-45d7-bcae-dfbb6d5456bd"],
Cell[5265, 176, 826, 27, 50, "Output",ExpressionUUID->"32c1c35e-18d4-4712-ab26-1570918273a3"],
Cell[6094, 205, 658, 18, 54, "Output",ExpressionUUID->"4f4c957f-32b9-4e5b-bcf8-70fbcfa30e56"],
Cell[CellGroupData[{
Cell[6777, 227, 507, 12, 23, "Print",ExpressionUUID->"9fa8dbf7-34c1-447e-a978-6f2a72943bda"],
Cell[7287, 241, 507, 12, 23, "Print",ExpressionUUID->"9a32af68-4aaf-4a63-b9c2-269feefdab7c"]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

