(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     20558,        605]
NotebookOptionsPosition[     19480,        580]
NotebookOutlinePosition[     19872,        596]
CellTagsIndexPosition[     19829,        593]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"Remove", "[", "\"\<Global`*\>\"", "]"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
   "The", " ", "symmetry", " ", "group", " ", "consists", " ", "initially", 
    " ", "only", " ", "of", " ", "its", " ", "generators"}], "*)"}]}], "\n", 
 RowBox[{
  RowBox[{"id", "=", 
   RowBox[{"IdentityMatrix", "[", "3", "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"list1", "=", 
    RowBox[{"{", "id", "}"}]}], ";"}], 
  RowBox[{"(*", "triclinic", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"list2", "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"-", "id"}], "+", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"2", ",", "0", ",", "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "}"}]}], "}"}]}], ";"}], 
  
  RowBox[{"(*", "monoclinic", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"list3", "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{
       RowBox[{"-", "id"}], "+", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"2", ",", "0", ",", "0"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "}"}]}], ",", 
      RowBox[{
       RowBox[{"-", "id"}], "+", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"0", ",", "2", ",", "0"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "}"}]}]}], "}"}]}], 
   ";"}], 
  RowBox[{"(*", "orthotropic", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"list4", "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"RotationMatrix", "[", 
       RowBox[{
        RowBox[{"2", 
         RowBox[{"Pi", "/", "3"}]}], ",", 
        RowBox[{"{", 
         RowBox[{"0", ",", "0", ",", "1"}], "}"}]}], "]"}], ",", 
      RowBox[{"RotationMatrix", "[", 
       RowBox[{"Pi", ",", 
        RowBox[{"{", 
         RowBox[{"1", ",", "0", ",", "0"}], "}"}]}], "]"}]}], "}"}]}], ";"}], 
  
  RowBox[{"(*", "trigonal", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"list5", "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"RotationMatrix", "[", 
       RowBox[{
        RowBox[{"Pi", "/", "2"}], ",", 
        RowBox[{"{", 
         RowBox[{"0", ",", "0", ",", "1"}], "}"}]}], "]"}], ",", 
      RowBox[{
       RowBox[{"-", "id"}], "+", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"2", ",", "0", ",", "0"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "}"}]}]}], "}"}]}], 
   ";"}], 
  RowBox[{"(*", "tetragonal", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"list6", "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"RotationMatrix", "[", 
       RowBox[{
        RowBox[{"Pi", "/", "3"}], ",", 
        RowBox[{"{", 
         RowBox[{"0", ",", "0", ",", "1"}], "}"}]}], "]"}], ",", 
      RowBox[{
       RowBox[{"-", "id"}], "+", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"2", ",", "0", ",", "0"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "}"}]}]}], "}"}]}], 
   ";"}], 
  RowBox[{"(*", "hexagonal", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"list7", "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"RotationMatrix", "[", 
       RowBox[{
        RowBox[{"Pi", "/", "2"}], ",", 
        RowBox[{"{", 
         RowBox[{"1", ",", "0", ",", "0"}], "}"}]}], "]"}], ",", 
      RowBox[{"RotationMatrix", "[", 
       RowBox[{
        RowBox[{"2", 
         RowBox[{"Pi", "/", "3"}]}], ",", 
        RowBox[{"{", 
         RowBox[{"1", ",", "1", ",", "1"}], "}"}]}], "]"}]}], "}"}]}], ";"}], 
  
  RowBox[{"(*", "cubic", "*)"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{"Example", " ", "for", " ", "cubic", " ", "symmetry"}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"list", "=", "list7"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"init", "=", 
   RowBox[{"Length", "[", "list", "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"outit", "=", "100000"}], ";"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
   "Generate", " ", "elements", " ", "until", " ", "no", " ", "new", " ", 
    "elements", " ", "appear"}], "*)"}]}], "\n", 
 RowBox[{
  RowBox[{"While", "[", 
   RowBox[{
    RowBox[{"init", "!=", "outit"}], ",", 
    RowBox[{
     RowBox[{"init", "=", 
      RowBox[{"Length", "[", "list", "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"For", "[", 
      RowBox[{
       RowBox[{"i", "=", "1"}], ",", 
       RowBox[{"i", "<=", "init"}], ",", 
       RowBox[{"i", "+=", "1"}], ",", 
       RowBox[{
        RowBox[{"For", "[", 
         RowBox[{
          RowBox[{"j", "=", "1"}], ",", 
          RowBox[{"j", "<=", "init"}], ",", 
          RowBox[{"j", "+=", "1"}], ",", 
          RowBox[{"AppendTo", "[", 
           RowBox[{"list", ",", 
            RowBox[{"FullSimplify", "[", 
             RowBox[{
              RowBox[{"list", "[", 
               RowBox[{"[", "i", "]"}], "]"}], ".", 
              RowBox[{"list", "[", 
               RowBox[{"[", "j", "]"}], "]"}]}], "]"}]}], "]"}]}], "]"}], 
        ";"}]}], "]"}], ";", "\[IndentingNewLine]", 
     RowBox[{"list", "=", 
      RowBox[{"DeleteDuplicates", "[", "list", "]"}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"outit", "=", 
      RowBox[{"Length", "[", "list", "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"Print", "[", 
      RowBox[{"\"\<Number of elements in G: \>\"", ",", "outit"}], "]"}]}]}], 
   "]"}], "\n", "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
   "Create", " ", "general", " ", "fourth", " ", "order", " ", "tensor"}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"c", "=", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"ToExpression", "[", 
       RowBox[{"StringJoin", "[", 
        RowBox[{"\"\<C\>\"", ",", 
         RowBox[{"ToString", "[", "i", "]"}], ",", 
         RowBox[{"ToString", "[", "j", "]"}], ",", 
         RowBox[{"ToString", "[", "k", "]"}], ",", 
         RowBox[{"ToString", "[", "l", "]"}]}], "]"}], "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"i", ",", "1", ",", "3"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"j", ",", "1", ",", "3"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"k", ",", "1", ",", "3"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"l", ",", "1", ",", "3"}], "}"}]}], "]"}]}], ";"}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{"Require", " ", "rotational", " ", "symmetries"}], 
   "*)"}]}], "\n", 
 RowBox[{
  RowBox[{"Q", "=", 
   RowBox[{"list", "[", 
    RowBox[{"[", "1", "]"}], "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"eqs1", "=", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"c", "[", 
       RowBox[{"[", 
        RowBox[{"m", ",", "n", ",", "o", ",", "p"}], "]"}], "]"}], "==", 
      RowBox[{"Sum", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"c", "[", 
          RowBox[{"[", 
           RowBox[{"i", ",", "j", ",", "k", ",", "l"}], "]"}], "]"}], 
         RowBox[{"Q", "[", 
          RowBox[{"[", 
           RowBox[{"m", ",", "i"}], "]"}], "]"}], " ", 
         RowBox[{"Q", "[", 
          RowBox[{"[", 
           RowBox[{"n", ",", "j"}], "]"}], "]"}], " ", 
         RowBox[{"Q", "[", 
          RowBox[{"[", 
           RowBox[{"o", ",", "k"}], "]"}], "]"}], " ", 
         RowBox[{"Q", "[", 
          RowBox[{"[", 
           RowBox[{"p", ",", "l"}], "]"}], "]"}]}], ",", 
        RowBox[{"{", 
         RowBox[{"i", ",", "1", ",", "3"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"j", ",", "1", ",", "3"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"k", ",", "1", ",", "3"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"l", ",", "1", ",", "3"}], "}"}]}], "]"}]}], ",", 
     RowBox[{"{", 
      RowBox[{"m", ",", "1", ",", "3"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"n", ",", "1", ",", "3"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"o", ",", "1", ",", "3"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"p", ",", "1", ",", "3"}], "}"}]}], "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"Q", "=", 
   RowBox[{"list", "[", 
    RowBox[{"[", "2", "]"}], "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"eqs2", "=", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"c", "[", 
        RowBox[{"[", 
         RowBox[{"m", ",", "n", ",", "o", ",", "p"}], "]"}], "]"}], "==", 
       RowBox[{"Sum", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"c", "[", 
           RowBox[{"[", 
            RowBox[{"i", ",", "j", ",", "k", ",", "l"}], "]"}], "]"}], " ", 
          RowBox[{"Q", "[", 
           RowBox[{"[", 
            RowBox[{"m", ",", "i"}], "]"}], "]"}], " ", 
          RowBox[{"Q", "[", 
           RowBox[{"[", 
            RowBox[{"n", ",", "j"}], "]"}], "]"}], " ", 
          RowBox[{"Q", "[", 
           RowBox[{"[", 
            RowBox[{"o", ",", "k"}], "]"}], "]"}], " ", 
          RowBox[{"Q", "[", 
           RowBox[{"[", 
            RowBox[{"p", ",", "l"}], "]"}], "]"}]}], ",", 
         RowBox[{"{", 
          RowBox[{"i", ",", "1", ",", "3"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"j", ",", "1", ",", "3"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"k", ",", "1", ",", "3"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"l", ",", "1", ",", "3"}], "}"}]}], "]"}]}], ",", 
      RowBox[{"{", 
       RowBox[{"m", ",", "1", ",", "3"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"n", ",", "1", ",", "3"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"o", ",", "1", ",", "3"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"p", ",", "1", ",", "3"}], "}"}]}], "]"}]}], ";"}], "\n", 
  RowBox[{"(*", 
   RowBox[{"Subsymmetries", " ", "and", " ", "principle", " ", "symmetry"}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"eqs4", "=", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"c", "[", 
       RowBox[{"[", 
        RowBox[{"m", ",", "n", ",", "o", ",", "p"}], "]"}], "]"}], "==", 
      RowBox[{"c", "[", 
       RowBox[{"[", 
        RowBox[{"m", ",", "n", ",", "p", ",", "o"}], "]"}], "]"}]}], ",", 
     RowBox[{"{", 
      RowBox[{"m", ",", "1", ",", "3"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"n", ",", "1", ",", "3"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"o", ",", "1", ",", "3"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"p", ",", "1", ",", "3"}], "}"}]}], "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"eqs5", "=", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"c", "[", 
        RowBox[{"[", 
         RowBox[{"m", ",", "n", ",", "o", ",", "p"}], "]"}], "]"}], "==", 
       RowBox[{"c", "[", 
        RowBox[{"[", 
         RowBox[{"o", ",", "p", ",", "m", ",", "n"}], "]"}], "]"}]}], ",", 
      RowBox[{"{", 
       RowBox[{"m", ",", "1", ",", "3"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"n", ",", "1", ",", "3"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"o", ",", "1", ",", "3"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"p", ",", "1", ",", "3"}], "}"}]}], "]"}]}], ";"}], "\n", 
  RowBox[{"(*", 
   RowBox[{"Summarize", " ", "equations"}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"eqs", "=", 
    RowBox[{"Flatten", "[", 
     RowBox[{"{", 
      RowBox[{"eqs1", ",", "eqs2", ",", "eqs4", ",", "eqs5"}], "}"}], "]"}]}],
    ";"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"3", "^", "4"}], " ", "minus", " ", "the", " ", "number", " ", 
    "of", " ", "independent", " ", "equations", " ", 
    RowBox[{"(", 
     RowBox[{
     "rank", " ", "of", " ", "the", " ", "coefficient", " ", "matrix"}], 
     ")"}], " ", "is", " ", "the"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{"number", " ", "of", " ", "independent", " ", "components", " ", 
    RowBox[{"w", ".", "r", ".", "t", ".", "the"}], " ", "symmetry", " ", 
    RowBox[{"group", "."}]}], "*)"}]}], "\n", 
 RowBox[{
  RowBox[{"Print", "[", 
   RowBox[{"\"\<Number of independent components: \>\"", ",", 
    RowBox[{"81", "-", 
     RowBox[{"MatrixRank", "[", 
      RowBox[{
       RowBox[{"CoefficientArrays", "[", 
        RowBox[{"eqs", ",", 
         RowBox[{"Flatten", "[", "c", "]"}]}], "]"}], "[", 
       RowBox[{"[", "2", "]"}], "]"}], "]"}]}]}], "]"}], 
  "\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{"Show", " ", "stiffness", " ", "without", " ", "dependent", " ", 
    RowBox[{"components", "."}]}], "*)"}]}], "\n", 
 RowBox[{
  RowBox[{"erg", "=", 
   RowBox[{"Solve", "[", 
    RowBox[{"eqs", ",", 
     RowBox[{"Flatten", "[", "c", "]"}]}], "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"Set", "@@@", 
   RowBox[{"erg", "[", 
    RowBox[{"[", "1", "]"}], "]"}]}], ";"}], "\n", "c", "\n"}], "Input",
 CellChangeTimes->{{3.8746787788441057`*^9, 3.874678778844816*^9}, {
  3.874678812813785*^9, 
  3.874678899886149*^9}},ExpressionUUID->"9b4e8ba5-ac7b-4006-a0a8-\
23ff30471de3"],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Number of elements in G: \"\>", "\[InvisibleSpace]", "6"}],
  SequenceForm["Number of elements in G: ", 6],
  Editable->False]], "Print",
 CellChangeTimes->{3.874678780269311*^9, 3.874678837462144*^9, 
  3.874678891690733*^9},
 CellLabel->
  "During evaluation of \
In[150]:=",ExpressionUUID->"393f9a9d-c27f-438a-a0c4-6bfbecf55405"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Number of elements in G: \"\>", "\[InvisibleSpace]", "18"}],
  SequenceForm["Number of elements in G: ", 18],
  Editable->False]], "Print",
 CellChangeTimes->{3.874678780269311*^9, 3.874678837462144*^9, 
  3.874678891692288*^9},
 CellLabel->
  "During evaluation of \
In[150]:=",ExpressionUUID->"62981d8a-6b27-45ed-81f6-c3119cfcb462"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Number of elements in G: \"\>", "\[InvisibleSpace]", "24"}],
  SequenceForm["Number of elements in G: ", 24],
  Editable->False]], "Print",
 CellChangeTimes->{3.874678780269311*^9, 3.874678837462144*^9, 
  3.874678891707711*^9},
 CellLabel->
  "During evaluation of \
In[150]:=",ExpressionUUID->"b300326d-2c74-4d19-aadd-2f8a843ff988"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Number of elements in G: \"\>", "\[InvisibleSpace]", "24"}],
  SequenceForm["Number of elements in G: ", 24],
  Editable->False]], "Print",
 CellChangeTimes->{3.874678780269311*^9, 3.874678837462144*^9, 
  3.8746788917242327`*^9},
 CellLabel->
  "During evaluation of \
In[150]:=",ExpressionUUID->"bc0aed74-c468-4be2-962f-05e47a46e181"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Number of independent components: \"\>", "\[InvisibleSpace]", 
   "3"}],
  SequenceForm["Number of independent components: ", 3],
  Editable->False]], "Print",
 CellChangeTimes->{3.874678780269311*^9, 3.874678837462144*^9, 
  3.874678891778804*^9},
 CellLabel->
  "During evaluation of \
In[150]:=",ExpressionUUID->"1ba8d117-7f1b-4296-a88b-e9ddd73c2607"]
}, Open  ]],

Cell[BoxData[
 TemplateBox[{
  "Solve", "svars", 
   "\"Equations may not give solutions for all \\\"solve\\\" variables.\"", 2,
    172, 50, 33978962891801377357, "Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{3.874678780208609*^9, 3.874678837438283*^9, 
  3.874678891889389*^9},
 CellLabel->
  "During evaluation of \
In[150]:=",ExpressionUUID->"e721402c-da45-4e33-96b6-88cd38e36cbd"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"C2222", ",", "0", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "C2233", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "C2233"}], "}"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"0", ",", "C2332", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"C2332", ",", "0", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "C2332"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"C2332", ",", "0", ",", "0"}], "}"}]}], "}"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"0", ",", "C2332", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"C2332", ",", "0", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"C2233", ",", "0", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "C2222", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "C2233"}], "}"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "C2332"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "C2332", ",", "0"}], "}"}]}], "}"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "C2332"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"C2332", ",", "0", ",", "0"}], "}"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "C2332"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "C2332", ",", "0"}], "}"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"C2233", ",", "0", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "C2233", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "C2222"}], "}"}]}], "}"}]}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{
  3.874678834674212*^9, {3.87467886511033*^9, 3.8746788919071712`*^9}},
 CellLabel->
  "Out[174]=",ExpressionUUID->"853c3ac9-5783-4b09-a39b-c07a45ea4f22"]
}, Open  ]]
},
WindowSize->{1440., 746.25},
WindowMargins->{{0, Automatic}, {0, Automatic}},
FrontEndVersion->"12.3 for Linux x86 (64-bit) (May 11, 2021)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"cf816c48-e3f6-4a06-b18e-5613578f388d"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 13587, 397, 883, "Input",ExpressionUUID->"9b4e8ba5-ac7b-4006-a0a8-23ff30471de3"],
Cell[CellGroupData[{
Cell[14192, 423, 381, 9, 23, "Print",ExpressionUUID->"393f9a9d-c27f-438a-a0c4-6bfbecf55405"],
Cell[14576, 434, 383, 9, 23, "Print",ExpressionUUID->"62981d8a-6b27-45ed-81f6-c3119cfcb462"],
Cell[14962, 445, 383, 9, 23, "Print",ExpressionUUID->"b300326d-2c74-4d19-aadd-2f8a843ff988"],
Cell[15348, 456, 385, 9, 23, "Print",ExpressionUUID->"bc0aed74-c468-4be2-962f-05e47a46e181"],
Cell[15736, 467, 403, 10, 23, "Print",ExpressionUUID->"1ba8d117-7f1b-4296-a88b-e9ddd73c2607"]
}, Open  ]],
Cell[16154, 480, 409, 10, 28, "Message",ExpressionUUID->"e721402c-da45-4e33-96b6-88cd38e36cbd"],
Cell[16566, 492, 2898, 85, 75, "Output",ExpressionUUID->"853c3ac9-5783-4b09-a39b-c07a45ea4f22"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

